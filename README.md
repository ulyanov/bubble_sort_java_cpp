# Experiment: Is C++ faster than Java for Bubble Sort?

## How to run
1. Make sure you have minimum versions of Java 8 and C++ 11 installed.
2. Run ```make all``` to build.
3. Run ```make run``` to run the tests.
4. Run ```python analyze.py``` to analyze the results (requires Python 3).