import java.io.*;

public class BubbleSort {

    public static void main(String[] args) {
        int[][] values = readInputFile("test_cases.csv", 40, 1000);

        long t0, t1;
        long[] results = new long[40];

        // sorted
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) { System.out.println("thread interrupted"); }
        for (int i = 0; i < 10; i++) {
            t0 = System.nanoTime();
            bubbleSort(values[i], 1000);
            t1 = System.nanoTime();
            results[i] = t1 - t0;
        }

        // reversed
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) { System.out.println("thread interrupted"); }
        for (int i = 10; i < 20; i++) {
            t0 = System.nanoTime();
            bubbleSort(values[i], 1000);
            t1 = System.nanoTime();
            results[i] = t1 - t0;
        }

        // half-sorted
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) { System.out.println("thread interrupted"); }
        for (int i = 20; i < 30; i++) {
            t0 = System.nanoTime();
            bubbleSort(values[i], 1000);
            t1 = System.nanoTime();
            results[i] = t1 - t0;
        }

        // random
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) { System.out.println("thread interrupted"); }
        for (int i = 30; i < 40; i++) {
            t0 = System.nanoTime();
            bubbleSort(values[i], 1000);
            t1 = System.nanoTime();
            results[i] = t1 - t0;
        }

        // save results to the file
        saveResultsToFile(results, "results_java.txt");
    }

    static void bubbleSort(int[] arr, int n) {
        int i, j, temp;
        for (i = 0; i < n-1; i++) {
            for (j = 0; j < n-i-1; j++) {
                if (arr[j] > arr[j+1]) {
                    temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    static int[][] readInputFile(String filename, int rows, int cols) {
        int[][] values = new int[rows][cols];

        try {
            File file = new File(filename);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            for (int i = 0; i < rows; i++) {
                String line = br.readLine();
                String[] parts = line.split(",");
                for (int j = 0; j < cols; j++) {
                    values[i][j] = Integer.parseInt(parts[j]);
                }
            }
            br.close();
        }
        catch (FileNotFoundException e) { System.out.println("file not found"); }
        catch (IOException e) { System.out.println("error while reading the file"); }

        return values;

    }

    static void saveResultsToFile(long[] results, String filename) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(filename));
            for (int i = 0; i < results.length; i++) {
                String line = Long.toString(results[i]);
                if (i < results.length) { line += "\n"; }
                fos.write(line.getBytes());
            }
            fos.close();
        }
        catch (FileNotFoundException e) { System.out.println("file was not found"); }
        catch (IOException e) { System.out.println("error writing file"); }
    }
}
