#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
using namespace std;

void bubbleSort(int arr[], int n)  
{
	int i, j, temp;
	for (i = 0; i < n-1; i++)
	{
		for (j = 0; j < n-i-1; j++)
		{
			if (arr[j] > arr[j+1])
			{
				temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

int main()  
{
	ifstream data("test_cases.csv");

	string line;
	int values[40][1000];

	int row_counter = 0;
	while (getline(data, line))
	{
		stringstream lineStream(line);
		string cell;

		int col_counter = 0;
		while (getline(lineStream, cell, ','))
		{
			values [row_counter][col_counter] = stoi(cell);
			col_counter++;
		}

		row_counter++;
	}

	data.close();

	ofstream outputFile("results_cpp.txt");

	// sorted
	sleep(1);
	for (int i=0; i<10; i++)
	{
		auto t0 = chrono::high_resolution_clock::now();
	    bubbleSort(values[i], 1000);
	    auto t1 = chrono::high_resolution_clock::now();
	    outputFile << chrono::duration_cast<chrono::nanoseconds>(t1 - t0).count() << endl;
	}

	// reversed
	sleep(1);
	for (int i=10; i<20; i++)
	{
		auto t0 = chrono::high_resolution_clock::now();
	    bubbleSort(values[i], 1000);
	    auto t1 = chrono::high_resolution_clock::now();
	    outputFile << chrono::duration_cast<chrono::nanoseconds>(t1 - t0).count() << endl;
	}

	// half-sorted
	sleep(1);
	for (int i=20; i<30; i++)
	{
		auto t0 = chrono::high_resolution_clock::now();
	    bubbleSort(values[i], 1000);
	    auto t1 = chrono::high_resolution_clock::now();
	    outputFile << chrono::duration_cast<chrono::nanoseconds>(t1 - t0).count() << endl;
	}

	// random
	sleep(1);
	for (int i=30; i<40; i++)
	{
		auto t0 = chrono::high_resolution_clock::now();
	    bubbleSort(values[i], 1000);
	    auto t1 = chrono::high_resolution_clock::now();
	    outputFile << chrono::duration_cast<chrono::nanoseconds>(t1 - t0).count() << endl;
	}

	outputFile.close();
	
    return 0;  
}