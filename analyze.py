import math

# read the results
results_java = []
with open("results_java.txt", "r") as f:
	line = f.readline()
	while(line):
		results_java.append(int(line))
		line = f.readline()

results_cpp = []
with open("results_cpp.txt", "r") as f:
	line = f.readline()
	while(line):
		results_cpp.append(int(line))
		line = f.readline()


# calculate differences
differences = []
for i in range(len(results_java)):
	differences.append(results_java[i] - results_cpp[i])

print("--------------------------------------------------------------")

# find percentage when C++ was faster
print("C++ is faster in percentage:")

# For pre-sorted tests
print("\t{}:\t{}%\t({} in {} tests)".format(
	"Pre-sorted",
	len([x for x in differences[0:10] if x > 0])*100 / len(results_java[0:10]),
	len([x for x in differences[0:10] if x > 0]), 10
))

# For reversed tests
print("\t{}:\t{}%\t({} in {} tests)".format(
	"Reversed",
	len([x for x in differences[10:20] if x > 0])*100 / len(results_java[10:20]),
	len([x for x in differences[10:20] if x > 0]), 10
))

# For half-sorted tests
print("\t{}:\t{}%\t({} in {} tests)".format(
	"Half-sorted",
	len([x for x in differences[20:30] if x > 0])*100 / len(results_java[20:30]),
	len([x for x in differences[20:30] if x > 0]), 10
))

# For random tests
print("\t{}:\t\t{}%\t({} in {} tests)".format(
	"Random",
	len([x for x in differences[30:40] if x > 0])*100 / len(results_java[30:40]),
	len([x for x in differences[30:40] if x > 0]), 10
))

print()

# Overall
print("\t{}:\t{}%\t({} in {} tests)".format(
	"Overall",
	len([x for x in differences if x > 0])*100 / len(results_java),
	len([x for x in differences if x > 0]), len(results_java)
))

print("--------------------------------------------------------------")

# Distribution
print("Distribution of speed performance of C++ over Java:")

num_bins = 10
min_difference = min(differences)
max_difference = max(differences)
bin_size = math.ceil((max_difference - min_difference) / num_bins)

bins = [0] * num_bins
for val in differences:
	index = (val - min_difference) // bin_size
	bins[index] += 1

bins_sum = sum(bins)
for i in range(len(bins)):
	val_range = "{:8} to {:8}".format(min_difference+bin_size*i, min_difference+bin_size*(i+1))
	density = bins[i]*100 / bins_sum
	bar = "*" * int(density / 2)
	print("\t{}:\t{}%\t|{}".format(val_range, density, bar))

print("--------------------------------------------------------------")



