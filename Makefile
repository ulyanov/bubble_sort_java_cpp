all :
	javac BubbleSort.java
	g++ BubbleSort.cpp -o BubbleSort

run :
	java BubbleSort
	./BubbleSort

clean :
	rm results_java.txt
	rm results_cpp.txt
	rm BubbleSort.class
	rm BubbleSort	